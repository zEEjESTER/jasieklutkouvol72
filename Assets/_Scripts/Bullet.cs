﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Bullet : MonoBehaviour {

    public GameObject towerPrefab;
    private GameObject createdTower;

    private void OnCollisionEnter(Collision collision)
    {
        Collider collider = collision.collider;
        if (collider.tag == "Tower")
        {
            //if hit a turret the turret is destroyed
            if (createdTower == null || createdTower != collider.gameObject)
            Destroy(collider.gameObject);
        }
        else if (ShouldSpawnNewTowers() && collider.tag == "Ground")
        {
            //creates a new turret where it hit 
            Debug.Log("New Tower Created");
            SpawnNewTower(collision.contacts[0].point);
        }
        //destroys itself
        Destroy(gameObject);
    }

    //will the bullet create a new tower when it hits the ground?
    private bool ShouldSpawnNewTowers()
    {
        if (GameController.instance.maxCountReached)
            return false;
        else
            return true;
    }

    //generating a new tower in the position where the bullet hit, no rotation
    private void SpawnNewTower(Vector3 position)
    {
        Vector3 spawnPosition = new Vector3(position.x, 2, position.z);
        createdTower = Instantiate(towerPrefab, spawnPosition, Quaternion.identity);
    }
}
