﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    #region Singleton
    public static GameController instance;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More then one instance of GameControler found! Better check that out!");
        }
        instance = this;

    }
    #endregion

    public List<Tower> allTowers = new List<Tower>();
    public bool maxCountReached;    //checks if the number of towers reached 100;

    //UI Text counting the towers
    [SerializeField]
    Text towersNumberText;

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        if (allTowers.Count >= 100 && maxCountReached == false)
        {
            
            maxCountReached = true;
            print("Max count reached");
            ResetAllTowers();
        }

        towersNumberText.text = ("Towers: " + allTowers.Count);
	}

    public void ResetAllTowers()
    {
        foreach (Tower tower in allTowers)
        {
            tower.shotCount = 0;    //reset the shotcount
            //changing the tower color to active - red
            tower.ChangeColor(Color.red);
            tower.StartCoroutine(tower.activeCoroutine);
            print("Resetting shot count for a tower");
        }
    }
}
