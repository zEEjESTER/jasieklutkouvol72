﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour {

    [SerializeField]
    private float timeDelay = 0.5f;

    public int shotCount = 0; //counts the number of shots fired by a given Tower
    public IEnumerator activeCoroutine;

    //additional variables
    public GameObject barrel;
    public GameObject barrelMuzzle;
    public GameObject bulletPrefab;
    public Material towerMaterial;
    public bool reachedFinalStage;

    private void Awake()
    {
        //Setting references to gameObjects
        barrel = transform.Find("Barrel").gameObject;
        barrelMuzzle = transform.Find("Barrel/Muzzle").gameObject;
    }

    void Start () {
        //Starting the rotation and fire coroutine
        activeCoroutine = RorateAndFire();
        StartCoroutine(activeCoroutine);
        GameController.instance.allTowers.Add(this);


        //setting the proper tower color at start
        towerMaterial = GetComponent<MeshRenderer>().material;
        ChangeColor(Color.red);
    }
    //Controlls the tower rotation and initiates shooting
    IEnumerator RorateAndFire()
    {
        while (true)
        {
            yield return new WaitForSeconds(timeDelay);
            float randomAngle = Random.Range(15f, 45f);
            float currentAngle = transform.rotation.eulerAngles.y;
            Quaternion newRotation = Quaternion.Euler(new Vector3(0, currentAngle + randomAngle, 0));
            //transform.rotation = newRotation;
            transform.eulerAngles = Vector3.Lerp(transform.rotation.eulerAngles, newRotation.eulerAngles, 0.5f);

            FireCannon();
        }
    }

    //Shoots a bullet
    public void FireCannon()
    {
        if (shotCount >= 12)
        {
            // stops shooting
            Debug.Log("All 12 shots fired");
            ChangeColor(Color.white);  // showing the Tower is inactive by changing it's color to white
            StopCoroutine(activeCoroutine);
        }
        else
        {
            //Instantiating a bullet at the muzzle position facing the same direction as the barrel of the Tower
            GameObject newBullet = Instantiate(bulletPrefab, barrelMuzzle.transform.position, barrel.transform.rotation);
            int bulletSpeed = Random.Range(1, 5);   //setting one of 4 possible bulet speeds at random
            newBullet.GetComponent<Rigidbody>().AddForce(barrelMuzzle.transform.up * bulletSpeed * 400);

            shotCount++;          
        }
        
    }

    public void ChangeColor(Color _color)
    {
        towerMaterial.color = _color;
        barrel.GetComponent<MeshRenderer>().material.color = _color;
    }

    //Remove destroyed towers from the Towers' list
    private void OnDestroy()
    {
        Debug.Log("Tower Destroyed!");
        GameController.instance.allTowers.Remove(this);
    }
}
